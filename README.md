# VanillaSoft
## Laravel Test
Cayetano H. Osma. <cayetano.hernandez.osma@gmail.com>

## Foreword
There is a dockerized containers which run the test. Thera are several containers as:
- cnt_web: Which runs the NGINX to serve the web pages. 
- cnt_php: Which runs the PHP server with Laravel 8.
- cnt_db: Runs the Mysql 5.7 engine to support the database.
- cnt_redis: I use Redis as cache system
- cnt_composer: Is a volatile container which install or update the requierd packages for the app.

Due to restrictions on Mailgun free testing platform, cannot send emails to unauthorized recipiends due to you should use your own mailgun account and you must change the following data at .env file

```
MAIL_MAILER=smtp  
MAIL_HOST=smtp.mailgun.org  
MAIL_PORT=2525  
MAIL_USERNAME=<you must fill it>  
MAIL_PASSWORD=<you must fill it>  
MAIL_ENCRYPTION=tls  
MAIL_FROM_ADDRESS=<you must fill it>  
MAIL_FROM_NAME="${APP_NAME}"
```  

## How to use
After cloning the repository, you must get inside the PHP container `cnt_php`

`docker exec -it cnt_php bash`

Once you're inside, you must change the permissions for the special Laravel's folders with 

`sudo chmod -R 755 storage`
`sudo chmod -R 755 bootstrap/cache`

and run the migrations in order to create the needed tables.

`php artisan migrate:install --env=local`
`php artisan migrate:refresh --env=local --seed`

To activate the queue and send the emails, you must run the following command inside the container

`php artisan queue:listen`

That will do the queued jobs.

After run the migrations and activate the queues, you can use POSTMAN to check the endpoints. Although, you can activate the queues whatever you want.

## Endpoints and environment to test
`/api/user`  
`/api/send`

In both cases you could use the configuration that is available in the repo as well

`Docker (localhost).postman_environment.json`  
This is the docker environment configuration. Here you must copy and paste a valid user api_token in order to be authenticated.
There is an environment variable ready to use for that.

`VanillaSoft.postman_collection.json`
These are the endpoints with some data ready to test. 
In this case remember that if you are using Mailgun, the recipients of the email must be authenticated at Mailgun in order to be used.
And you must replace the api_token with a valid user's api_token to be authenticated at the system.

## Test the application using PHPUnit.

To run the test just write `phpunit` inside the cnt_php container at the root folder of project




