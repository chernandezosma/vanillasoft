<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VanillaMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Email details
     *
     * @var array
     */
    protected $details;

    /**
     * Create a new message instance.
     *
     * @param $details array with the email details
     */
    public function __construct(array $details)
    {
        $this->details = $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // We setup the basics data for email.
        $mailableObject = $this->from('test-endpoint@cayetano.h.osma.com', 'Cayetano H. Osma.')
                               ->subject('Test Queued Email')
                               ->view('emails.vanillaemail', $this->details);

        // Run over all emails recipients.
        // NOTE: Remember that Mailgun don't allow to send email to address that won't be authorized.
        // You must authorize the recipients.
        foreach ($this->details['emails'] as $email) {
            $mailableObject->to($email);
        }

        // Due to test restriction, we need to convert the base64 data to a memory-file in order to attach to the
        // email body, and this operation must be done with all attachments in attachment array.
        foreach ($this->details['attachments'] as $attachment) {
            $mailableObject->attachData(base64_decode($attachment['data']), $attachment['name']);
        }

        return $mailableObject;
    }
}