<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmailJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{
    /**
     * Show the authenticated user
     *
     * @author  Cayetano H. Osma <cayetano.hernandez.osma@gmail.com>
     * @version Jan 2021
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUser(Request $request)
    {
        return response()->json($request->user());
    }

    /**
     * Sends the email to the email address given.
     *
     * @author  Cayetano H. Osma <cayetano.hernandez.osma@gmail.com>
     * @version Jan 2021
     *
     * @param Request $request
     */
    public function sendEmail(Request $request)
    {
        $validator = Validator::make($request->request->all(), [
            'emails'           => 'required|array|max:255',
            'emails.*'         => 'required|email|distinct',
            'title'            => 'string|min:5|max:255',
            'subject'          => 'required|string|min:5',
            'body'             => 'required|string',
            'attachments'      => 'array|max:10',
            'attachments.data' => 'string',
            'attachments.name' => 'string|max:255',

        ]);

        if ($validator->fails()) {
            return response([
                'status'  => 'Failed',
                'message' => 'The data validation fails',
                'data'    => $validator->errors()->all(),
            ], 400,
                ['Content-Type' => 'application/json']);
        }

        $details = [
            'emails'      => $request->request->get('emails', []),
            'title'       => $request->request->get('title', 'Endpoint testing email'),
            'subject'     => $request->request->get('subject'),
            'body'        => strip_tags($request->request->get('body')),
            'attachments' => $request->request->get('attachments', []),
        ];

        dispatch(new SendEmailJob($details));

        return response()->json([
            'status'  => 'Sent',
            'message' => "Email is Sent.",
        ]);
    }
}
