<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            // This is not the Laravel-Way to capture the error, but for the test I think that is enough
            // because we don't write a user's CRUD and Authentication / Authorization system.
            return '/';
        }
    }
}
