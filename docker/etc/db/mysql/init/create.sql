CREATE DATABASE IF NOT EXISTS `cnt` DEFAULT
    CHARACTER SET utf8mb4
    COLLATE utf8mb4_unicode_ci;

SET PASSWORD FOR 'root'@'localhost' = PASSWORD('root');

CREATE USER IF NOT EXISTS `dbuser`@`%` IDENTIFIED BY 'dbuser';
GRANT ALL PRIVILEGES ON cnt.* TO `dbuser`@`%`;
FLUSH PRIVILEGES;