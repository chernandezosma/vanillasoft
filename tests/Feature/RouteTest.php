<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RouteTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test the api/user route
     *
     * @return void
     */
    public function test_user_route()
    {
        $user = User::factory()->create();
        $route = '/api/user';
        $getQueryParams = 'api_token=' . $user->api_token;
        $uri = sprintf ('%s?%s', $route, $getQueryParams);
        $response = $this
            ->actingAs($user)
            ->withHeaders([
                'Accept' => 'application/json',
            ])
            ->get($uri);

        $response->assertStatus(200);
    }

    /**
     * Test the email route GET is not allowed verb, so it should return 405 Methos not allowed.
     *
     * @author  Cayetano H. Osma <cayetano.hernandez.osma@gmail.com>
     * @version Jan 2021
     */
    public function test_email_get_route()
    {
        $user = User::factory()->create();
        $route = '/api/send';
        $getQueryParams = 'api_token=' . $user->api_token;
        $uri = sprintf ('%s?%s', $route, $getQueryParams);
        $response = $this
            ->actingAs($user)
            ->withHeaders([
                'Accept' => 'application/json',
            ])
            ->get($uri);

        $response->assertStatus(405);
    }

    /**
     * Test the post without body, so it should return a 400.
     *
     * @author  Cayetano H. Osma <cayetano.hernandez.osma@gmail.com>
     * @version Jan 2021
     *
     */
    public function test_email_post_route()
    {
        $user = User::factory()->create();
        $route = '/api/send';
        $getQueryParams = 'api_token=' . $user->api_token;
        $uri = sprintf ('%s?%s', $route, $getQueryParams);
        $response = $this
            ->actingAs($user)
            ->withHeaders([
                'Accept' => 'application/json',
            ])
            ->post($uri, []);

        $response->assertStatus(400);
    }

    /**
     * Test to check the data is not completely filled.
     * Subject, body and title are not present
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2021
     *
     */
    public function test_email_post_route_with_not_present_attributes()
    {
        $user = User::factory()->create();
        $route = '/api/send';
        $getQueryParams = 'api_token=' . $user->api_token;
        $uri = sprintf ('%s?%s', $route, $getQueryParams);
        $response = $this
            ->actingAs($user)
            ->withHeaders([
                'Accept' => 'application/json',
            ])
            ->post($uri, [
                'emails' => [
                    'moesis@gmail.com',
                    'chernandez@elestadoweb.com'
                ],
            ]);

        $response->assertExactJson([
            'status' => 'Failed',
            'message' => 'The data validation fails',
            'data' => [
                "The subject field is required.",
                "The body field is required."
            ]
        ]);
        $response->assertStatus(400);
    }

    /**
     * Test to check the data is not completely filled.
     * Subject, body and title are not filled
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2021
     *
     */
    public function test_email_post_route_with_not_filled_attributes()
    {
        $user = User::factory()->create();
        $route = '/api/send';
        $getQueryParams = 'api_token=' . $user->api_token;
        $uri = sprintf ('%s?%s', $route, $getQueryParams);
        $response = $this
            ->actingAs($user)
            ->withHeaders([
                'Accept' => 'application/json',
            ])
            ->post($uri, [
                'emails' => [
                    'moesis@gmail.com',
                    'chernandez@elestadoweb.com'
                ],
                'subject' => '',
                'body' => '',
                'title' => ''
            ]);

        $response->assertExactJson([
            'status' => 'Failed',
            'message' => 'The data validation fails',
            'data' => [
                "The title must be a string.",
                "The title must be at least 5 characters.",
                "The subject field is required.",
                "The body field is required."
            ]
        ]);

        $response->assertStatus(400);
    }

    /**
     * Test that title has not the minimum length
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2021
     *
     */
    public function test_email_post_route_with_title_error()
    {
        $user = User::factory()->create();
        $route = '/api/send';
        $getQueryParams = 'api_token=' . $user->api_token;
        $uri = sprintf ('%s?%s', $route, $getQueryParams);
        $response = $this
            ->actingAs($user)
            ->withHeaders([
                'Accept' => 'application/json',
            ])
            ->post($uri, [
                'emails' => [
                    'moesis@gmail.com',
                    'chernandez@elestadoweb.com'
                ],
                'subject' => 'subject text',
                'body' => 'body text',
                'title' => ''
            ]);

        $response->assertExactJson([
            'status' => 'Failed',
            'message' => 'The data validation fails',
            'data' => [
                "The title must be a string.",
                "The title must be at least 5 characters."
            ]
        ]);

        $response->assertStatus(400);
    }

    /**
     * Test to check that the request cannot be done without body attribute
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2021
     *
     */
    public function test_email_post_route_with_body_error()
    {
        $user = User::factory()->create();
        $route = '/api/send';
        $getQueryParams = 'api_token=' . $user->api_token;
        $uri = sprintf ('%s?%s', $route, $getQueryParams);
        $response = $this
            ->actingAs($user)
            ->withHeaders([
                'Accept' => 'application/json',
            ])
            ->post($uri, [
                'emails' => [
                    'moesis@gmail.com',
                    'chernandez@elestadoweb.com'
                ],
                'subject' => 'Subject text',
                'body' => '',
                'title' => 'title text'
            ]);

        $response->assertExactJson([
            'status' => 'Failed',
            'message' => 'The data validation fails',
            'data' => [
                "The body field is required."
            ]
        ]);

        $response->assertStatus(400);
    }

    /**
     * Test to check the data is not completely filled.
     * Subject, body and title are not filled
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2021
     *
     */
    public function test_email_post_route_with_no_subject_filled()
    {
        $user = User::factory()->create();
        $route = '/api/send';
        $getQueryParams = 'api_token=' . $user->api_token;
        $uri = sprintf ('%s?%s', $route, $getQueryParams);
        $response = $this
            ->actingAs($user)
            ->withHeaders([
                'Accept' => 'application/json',
            ])
            ->post($uri, [
                'emails' => [
                    'moesis@gmail.com',
                    'chernandez@elestadoweb.com'
                ],
                'subject' => '',
                'body' => 'The body text',
                'title' => 'The Title text'
            ]);

        $response->assertExactJson([
            'status' => 'Failed',
            'message' => 'The data validation fails',
            'data' => [
                "The subject field is required.",
            ]
        ]);

        $response->assertStatus(400);
    }

    /**
     * Test to check the email recipients are not present
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2021
     *
     */
    public function test_email_post_route_with_no_emails()
    {
        $user = User::factory()->create();
        $route = '/api/send';
        $getQueryParams = 'api_token=' . $user->api_token;
        $uri = sprintf ('%s?%s', $route, $getQueryParams);
        $response = $this
            ->actingAs($user)
            ->withHeaders([
                'Accept' => 'application/json',
            ])
            ->post($uri, [
                'emails' => [],
                'subject' => 'subject text',
                'body' => 'The body text',
                'title' => 'The Title text'
            ]);

        $response->assertExactJson([
            'status' => 'Failed',
            'message' => 'The data validation fails',
            'data' => [
                "The emails field is required.",
            ]
        ]);

        $response->assertStatus(400);
    }

    /**
     * Test to check the email recipients are not present
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2021
     *
     */
    public function test_email_post_route_with_no_emails_attribute()
    {
        $user = User::factory()->create();
        $route = '/api/send';
        $getQueryParams = 'api_token=' . $user->api_token;
        $uri = sprintf ('%s?%s', $route, $getQueryParams);
        $response = $this
            ->actingAs($user)
            ->withHeaders([
                'Accept' => 'application/json',
            ])
            ->post($uri, [
                'subject' => 'subject text',
                'body' => 'The body text',
                'title' => 'The Title text'
            ]);

        $response->assertExactJson([
            'status' => 'Failed',
            'message' => 'The data validation fails',
            'data' => [
                "The emails field is required.",
            ]
        ]);

        $response->assertStatus(400);
    }

    /**
     * Test to check the email recipients are not present
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2021
     *
     */
    public function test_email_post_route_with_malformed_emails()
    {
        $user = User::factory()->create();
        $route = '/api/send';
        $getQueryParams = 'api_token=' . $user->api_token;
        $uri = sprintf ('%s?%s', $route, $getQueryParams);
        $response = $this
            ->actingAs($user)
            ->withHeaders([
                'Accept' => 'application/json',
            ])
            ->post($uri, [
                'emails' => [
                    'jhondoe',
                    'jhondoe@',
                    'jhondoe@unknownl.com',
                ],
                'subject' => 'subject text',
                'body' => 'The body text',
                'title' => 'The Title text'
            ]);

        $response->assertExactJson([
            'status' => 'Failed',
            'message' => 'The data validation fails',
            'data' => [
                "The emails.0 must be a valid email address.",
                "The emails.1 must be a valid email address.",
            ]
        ]);

        $response->assertStatus(400);
    }

}
